<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Clamanthook</title>
	<link rel="stylesheet" href="assets/css/owl.carousel.min.css">
	<link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="style.css">
	<link rel="stylesheet" type="text/css" href="media.css">

</head>
<body>
	<div class="container">
		<div class="header-nav" id="home">
			<nav class="navbar navbar-expand-lg nav-bg-color">
			  <a class="navbar-brand brand-name" href="#">Clamanthook</a>
			  <button class="navbar-toggler mobile-hamburger" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			    <span class="navbar-toggler-icon hamburgerIcon"></span>
			  </button>
			  <div class="collapse navbar-collapse nav-elements" id="navbarSupportedContent">
			    <ul class="navbar-nav ml-auto">
					<li class="nav-item active">
						<a class="nav-link" href="#home">Home</a>
					</li>
					<li class="nav-item active">
						<a class="nav-link" href="#ourServices">Services</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#contactUS">Contact</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#aboutUS">About us</a>
					</li>
			    </ul>
			  </div>
			</nav>
		</div>
		<div class="owl-carousel owl-theme">
		    <div class="item">
		    	<img src="assets/images/b06.jpg">
		    </div>
		    <div class="item">
		    	<img src="assets/images/b03.jpg">
		    </div>
		    <div class="item">
		    	<img src="assets/images/b02.jpg">
		    </div>
		    <div class="item">
		    	<img src="assets/images/b01.jpg">
		    </div>
		</div>
		<div class="our-services" id="ourServices">
			<div class="row">
				<div class="col-sm-12">
					<h2>Our Services</h2>	
				</div>
			</div>
			<div class="row">
				<div class="services">
					<ul class="nav nav-tabs service-head" id="myTab" role="tablist">
					  	<li class="nav-item">
					    	<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Web Development</a>
					   </li>
						<li class="nav-item">
						    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">App Development</a>
						</li>
						<li class="nav-item">
						    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Digital Marketing</a>
						</li>
					</ul>
					<div class="tab-content services-content" id="myTabContent">
					    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
					    	<p>
					    		Our developers work closely with you to develop the right software solution for your business.

								From the conceptualization of the project to its development, we work in partnership with you to deliver an end-to end solution for your business.
								Our services cover the full software development life cycle. From pre-launch development, prototyping, code deployment and testing through to post-launch maintenance and support.
					    	</p>
					    </div>
					  	<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
					  		<p>
					  			We deliver a software package for your business that address your business needs. 
								Our experts create your premium Hybrid mobile application Development, Testing and Online Support.
					  		</p>
					  	</div>
					  	<div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
					  		<div class="digital-content">
					  			<h2>“LET US HELP YOU GROW YOUR SALES”</h2>

								<p>Our Clamant Hook experts can help you in finding your target audience, measure the interest in your products & services, and finally benefit from all that. We can assist you in attracting new prospects to your website by optimizing your website and making people aware that your site exists while building brand awareness and Marketing services like SEO, Branding and Identity, Instagram Marketing, Paid Advertising, Email Marketing, Influencer Marketing, Website Design & Development.</p>

								<h2>Search Engine Optimization (SEO)</h2>
								<p>Thousands of digital searches are carried out by prospects every second. With our SEO Services, our target is to make your website a primary destination for your target audience thereby getting you a share of those digital searches.<p>

								<h2>Website Design and Website Development</h2>
								<p>We provide exquisite websites designed and developed by our specialized development team.</p>

								<h2>Branding and Identity</h2>
								<p>?Your brand identity needs to reflect the vision of your organization and communicate how you will make their life better. Our approach starts with understanding current customer perceptions of the problem your organization is solving and where there is a gap in terms of customer requirements.</p>

								<h2>Instagram Marketing</h2>
								<p>Your Instagram handle will be constructively handled by our experts in providing a platform for extensive marketing on Social Media.</p>

								<h2>Email Marketing</h2>
								<p>We will develop informative and captivating programmed Emails to help you absorb as many customers as possible.</p>

								<h2>Paid Advertising</h2>
								<p>Your website will be made available for advertising as we will procure and attract your potential customers through various platforms like “Facebook Ads” and “Google Ads”.
								Influencer Marketing
								We will work and align your business focus along with the right mastery to ease your hold over your target customers.</p>
					  		</div>
					  	</div>
					</div>
				</div>
			</div>
		</div>
		<div class="contact-us" id="aboutUS">
			<div class="row">
				<h2 class="text-uppercase">Who We Are</h2>
			</div>
			<div class="row">
				<div class="col-sm-12 contact-content">
					<p><b>Clamanthook</b> is a Bengaluru and Mysuru based Software Company which provides IT services. The professional company is closely structured in such a way to offer more acquirable results and solutions for use in a variety of businesses, encountering problems in information and technology sector. Work that makes an impact on our clients' business and, hopefully, the world. We do it by bringing bright people together from all walks of life and every imaginable background, letting them loose on a challenge, and giving them the freedom to explore the possibilities.</p>
					<h4><b>We find solutions that simplify</b></h4>
					<p>We find ways to simplify complex processes, so we can help eliminate worry and frustration at work and in life. We want work to feel less like work, so everyone can focus on what’s most important.</p>
					<h4><b>Our Approach</b></h4>
					<ul>
						<li> Think</li>
						<li> Generate</li>
						<li> Create</li>
						<li> Activate</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="get-in-touch" id="contactUS">
			<div class="row">
				<div class="row">
					<div class="col-sm-12 ">
						<h2>Have a Project ? <span>Let's Talk</span></h2>
					</div>
				</div>
			</div>
			<form action="connection.php" method="post">
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="exampleFormControlTextarea1">Name</label>
					    	<input name="customerName" type="name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter full name">
					  	</div>
					  	<div class="form-group">
					  		<label for="exampleFormControlTextarea1">Email</label>
					    	<input name="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
					  	</div>
					  	<div class="form-group">
						    <label for="exampleFormControlSelect1">Annual Revenue</label>
						    <select name="customerRevenue" class="form-control" id="exampleFormControlSelect1">
						      <option>100000</option>
						      <option>200000</option>
						      <option>300000</option>
						      <option>400000</option>
						      <option>500000</option>
						    </select>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
						    <label for="exampleFormControlTextarea1">Where did you hear about us?</label>
						    <textarea name="hear" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
						</div>
						<div class="form-group">
						    <label for="exampleFormControlTextarea1">Project Details</label>
						    <textarea name="project" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
						</div>
					</div>
				</div>
				<div class="row justify-content-end">
					<button name="add" id="add" type="submit" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Submit</button>
				</div>
			</form>
		</div>
		<footer class="footer-class">
			<div class="footer-front">
				<div class="row">
					<div class="col-sm-6">
						<h2>OFFICE ADDRESS</h2>
						<address>#367, 1st Level, 4th Stage</address>
						<address>Vijaynagar, Mysore - 570018</address>
						<address>Karnataka, India</address>
					</div>
					<div class="col-sm-6">
						<h2>Email</h2>
						<h4>info@clamanthook.com</h4>
					</div>
				</div>	
			</div>
			<div class="copy-rights">
				<div class="row">
					<h3>© 2019 All Rights Reserved</h3>
				</div>
			</div>
		</footer>
	</div>
	<script type="text/javascript" src="assets/js/jquery-3.3.1.slim.min.js"></script>
	<script type="text/javascript" src="assets/js/popper.min.js"></script>
	<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/owl.carousel.js"></script>
	<script type="text/javascript" src="site.js"></script>

</body>
</html>