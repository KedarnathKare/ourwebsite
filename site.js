$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
    dots:false,
    autoplay: true,
    responsive:{
        0:{
            items:1
        }
    }
});
function myOnloadFunc() {
    $('#success_msg').modal('show');
}